var lampotila2015 = [1, 3, 4, 7, -1, 2, 0, -5, -12, 3, 6, -7];
var lampotila2016 = [2, -3, 4, 3, -10, 4, 1, -6, -2, 13, -6, 2];

var newTaulu2015 = lampotila2015.filter(lampotila => lampotila > 0);
var newTaulu2016 = lampotila2016.filter(lampotila => lampotila > 0);

const keskiarvo2015  = (newTaulu2015.reduce((acc, lampotila) => acc + lampotila, 0)) / newTaulu2015.length;
const keskiarvo2016  = (newTaulu2016.reduce((acc, lampotila) => acc + lampotila, 0)) / newTaulu2016.length;

console.log(keskiarvo2015);
console.log(keskiarvo2016);