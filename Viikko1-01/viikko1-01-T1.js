var merkkijono = "imaami";
onPalindromi(merkkijono);


function onPalindromi(merkkijono){
    var merkkijonoTaulu = [];
    var merkkijonoPituus = merkkijono.length;

    for(var i = 0; i < merkkijono.length; i++){
        merkkijonoTaulu[i] = merkkijono.charAt(i);
    }

    if(merkkijonoPituus == 0 || merkkijonoPituus == 1){
        console.log("On Palindromi");
        return true;
    }else if(merkkijonoTaulu[0] === merkkijonoTaulu[merkkijonoPituus-1]){
        var uusiMerkkijono = "";
        for(var i = 1; i < merkkijonoPituus - 1; i++){
            uusiMerkkijono += merkkijonoTaulu[i];
        }
        onPalindromi(uusiMerkkijono);
    }else{
        console.log("Ei ole palindromi");
        return false;       
    }
}