var lista = [0,1,2,3,4,5,6,7,8,9];
var counter = 0;
console.log("Alkuperäinen lista:");
console.log(lista);
käännäLista(lista);

function käännäLista(lista){
    if(counter === Math.floor(lista.length/2)){
        console.log("Käännetty lista:");
        console.log(lista);
    }else{
        var ensimmäinenLuku = lista[counter];
        var viimeinenLuku = lista[lista.length - (1+counter)];

        lista[counter] = viimeinenLuku;
        lista[lista.length - (1 + counter)] = ensimmäinenLuku;
        counter ++;

        käännäLista(lista);
    }
    
}
