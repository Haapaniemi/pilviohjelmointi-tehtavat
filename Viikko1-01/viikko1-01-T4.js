var counter = 0;
rekursiivinenPotenssi(2);

function rekursiivinenPotenssi(luku){
    counter ++;
    if(counter > 5){
        console.log(luku);
    }else{
        var uusiLuku = Math.pow(luku, 2);
        rekursiivinenPotenssi(uusiLuku);
    }
}