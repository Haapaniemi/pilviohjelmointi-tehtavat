var lazyAdding = (x,y) => () => x + y;

var result = lazyAdding(1,2); // tässä vaiheessa 1 + 2 laskua ei vielä lasketa

console.log(result()); // vasta kun kutsutaan funktiota lasketaan lasku operaatio 1 + 2

var notLazyAdding = (x,y) => x + y;

var result_2 = notLazyAdding(1,2); // Tässä kohtaa suoritetaan jo lasku operaatio 1 + 2

console.log(result_2); // Siksi ei tässä kutsuta funktiota