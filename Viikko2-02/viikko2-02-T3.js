

const Auto = (function() {
    const suojattu = new WeakMap();
    const Immutable = class Auto {
        constructor(p_tankki, p_matkamittari) {
            suojattu.set(this, {matkamittari: p_matkamittari});
            this.tankki = p_tankki;
            if (new.target === Immutable) {
                Object.freeze(this);
            }
        }
    
        getMatkamittariLukema() {
            return suojattu.get(this).matkamittari;
        }
    
        getTankkiLukema() {
            return this.tankki;
        }
    
        aja() {
            var uusiLukema = 0;
            uusiLukema = suojattu.get(this).matkamittari;
            uusiLukema += 100;
            suojattu.set(this, {matkamittari: uusiLukema}); 
            this.tankki -= 8;
        }
    
        tankkaa() {
            this.tankki = 60;
        }
    }
    return Immutable;
})();

const Immutable = require('immutable');

const auto = new Auto(60, 0);

console.log("matkamittarin lukema = " + auto.getMatkamittariLukema());
console.log("Tankin lukema = " + auto.getTankkiLukema());
console.log("Ajetaan autolla 100km");
auto.aja();
console.log("matkamittarin lukema = " + auto.getMatkamittariLukema());
console.log("Tankin lukema = " + auto.getTankkiLukema());
console.log("Tankataan tankki täyteen.")
auto.tankkaa();
console.log("Tankin lukema = " + auto.getTankkiLukema());
auto.tankki = 100;
console.log("Tankin lukema = " + auto.getTankkiLukema());
