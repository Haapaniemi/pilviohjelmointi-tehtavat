const Immutable = require('immutable');

const set1 = Immutable.Set(['punainen', 'vihrea', 'keltainen']);

const set2 = set1.withMutations(
    function(set){
        set.add('ruskea');
    }
);
console.log("set1 = " + set1);
console.log("set2 = " + set2);
console.log(set1 === set2); //false

const set3 = set2.withMutations(
    function(set){
        set.add('ruskea');
    }
);

console.log("set2 = " + set2);
console.log("set3 = " + set3);
console.log(set2 === set3); //true, ruskeaa ei lisätä kahdesti

