function laskePisteet(pituus, kPiste, lisapisteet) {
    if(pituus >= kPiste) {
        pisteet = 60;
        
        if(pituus > kPiste) {
            lisamatka = pituus - kPiste;
            pisteet = pisteet + lisamatka * lisapisteet;
        }
    }
    return pisteet;
}

function laskePisteetCur(kPiste, lisapisteet) {
    return function(pituus) {
        if(pituus >= kPiste) {
            var pisteet = 60;
            
            if(pituus > kPiste) {
                lisamatka = pituus - kPiste;
                pisteet = pisteet + (lisamatka * lisapisteet);
            }
        } else if (pituus < kPiste) {
            var pisteet = 0;
            ero = kPiste - pituus;
            pisteet = pisteet - (ero * lisapisteet); 
        }
        return pisteet;
    }
}

const normaaliLahti = laskePisteetCur(80, 2.0);
let pisteet = normaaliLahti(98);


var lahdenHypyt = [60, 80, 90, 110, 75, 80];

const map = lahdenHypyt.map(x => console.log(normaaliLahti(x)));