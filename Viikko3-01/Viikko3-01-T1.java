public class JavaApplication30 {
    
    interface Calculations {
        Double doConversion(long fahrenheit);
    }
    
    interface Calculations2 {
        Double area(long radius);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Calculations toCelsius = (fahrenheit) -> fahrenheit - 32 / 1.8;

        Calculations2 area = (radius) -> Math.PI * radius * radius;

        System.out.println(toCelsius.doConversion(100));
        System.out.println(area.area(10));

    }

}