        // Kysely 8: Kaikki rahansiirrot, jotka tehty vuoden 2012 jälkeen ja arvo
        // vähintään 900.
        // PuttingIntoPractice.java
        List<Transaction> tr2012 = transactions.stream()
                                               .filter(transaction -> transaction.getYear() == 2012 && transaction.getValue() >= 900)
                                               .sorted(comparing(t -> t.getValue()))
                                               .collect(toList());
        System.out.println(tr2012);


        // Dish.java
        public static void main(String ...args){
            int ruokalajienMaara = (int) menu.stream()
                .map(m -> m.getType())
                .count();
        
            System.out.println(ruokalajienMaara);
        }
                