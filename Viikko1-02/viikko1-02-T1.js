function outerFunction(){

    function innerFunction(a,b){
        if(a > b){
            return 1;
        }else if(b > a){
            return -1;
        }else{
            return 0;
        }
    }

    return innerFunction;
}

var c = outerFunction();
var result = c(1,2);
console.log(result);