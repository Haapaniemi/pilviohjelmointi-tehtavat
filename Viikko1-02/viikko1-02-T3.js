'use strict'

const offset = [1,2];
const zoom = 2;
const one = Math.cos(Math.phi);
const two = Math.sin(Math.phi);

const point = { x: 1, y: 1};

const pipeline  = [   // 2D-muunnoksia
    
    function translate(p){
        return { x: p.x + offset[0], y: p.y + offset[1] };
    },

    function scale(p){
        return { x: p.x * zoom, y: p.y * zoom};
    },

    function rotate(p){
       // Ei toimi jostain kumman syystä? 
       //return { x: p.x * Math.cos(Math.phi) - p.y * Math.sin(Math.phi), y: p.x * Math.sin(Math.phi) + p.y * Math.cos(Math.phi)};
        return { x: p.x * -1, y: p.y * -1};
    }
];


function muunnos(point){
     for(let i=0; i<pipeline.length; i++){   
        point = pipeline[i](point);
    }
    return point;
}


console.log(point);
console.log(muunnos(point));