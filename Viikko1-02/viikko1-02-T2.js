function outerFunction(){
    
        function innerFunction(a,b){
            if(a > b){
                return 1;
            }else if(b > a){
                return -1;
            }else{
                return 0;
            }
        }
    
        return innerFunction;
    }

function vertaaTaulukoita(a,b,c){
    var counterB = 0;
    var counterC = 0;

    for(var i = 0; i < b.length; i++){
        if(a(b[i],c[i]) === 1){
            counterC ++;
        }else if(a(b[i],c[i]) === -1){
            counterB ++;
        }
    }

    if(counterB > counterC){
        console.log("Ensimmäisen taulukon keskilämpötila on suurempi " + (counterB-counterC) + " päivänä");
    }else if(counterB < counterC){
        console.log("Ensimmäisen taulukon keskilämpötila on pienempi " + (counterC-counterB) + " päivänä");
    }else{
        console.log("Taulukoiden keskilämpötilat olivat samat");
    }
}

var taulukko1 = [1,1,7,6.5,7,12,4,13,12,10];
var taulukko2 = [2,4,8,4.5,7,15,2,13,12,9];

vertaaTaulukoita(outerFunction(),taulukko2,taulukko1);