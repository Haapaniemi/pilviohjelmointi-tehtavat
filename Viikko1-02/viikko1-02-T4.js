function tailrecursive(luku, kerrat){
    if(kerrat === 0){
        console.log(luku);
    }else{
        return tailrecursive(Math.pow(luku,2), kerrat - 1);
    }
}

tailrecursive(2, 2);
