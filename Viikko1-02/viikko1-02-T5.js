'use strict';


var Moduuli = (function() {
  let x;
  var kasvata = function() { return ++x; };
  var vahenna = function() { return --x; };
  x = 1;

  return{
      kasvata: kasvata,
      vahenna: vahenna
  }
})();

console.log(Moduuli.kasvata());
console.log(Moduuli.kasvata());  
console.log(Moduuli.vahenna()); 